﻿int SumEvenNumber(int number)
{
    int sum = 0;
    if (number <= 1)
    {
        return 0;
    }
    else
    {
        for (int i = 2; i <= number; i++)
        {
            if(i%2 == 0)
            {
                sum += i;
            }
        }
    }
    return sum;
}
Console.Write("Enter your number: ");
int number = int.Parse(Console.ReadLine());

int result = SumEvenNumber(number);
if(result == 0)
{
    Console.WriteLine("Your number is invalid");
}
else
{
    Console.WriteLine("Sum of even number from 1 to " + number + " is " + SumEvenNumber(number));
}