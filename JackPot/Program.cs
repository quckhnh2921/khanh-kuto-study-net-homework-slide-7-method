﻿using System.Xml.Serialization;
int choice = 0;
int[] existLottery = { 662890 };
void PrintLottery()
{
    foreach (var item in existLottery)
    {
        Console.WriteLine(item);
    }
}
int AddNewLottery(int[] tempArr)
{
    Array.Resize(ref existLottery, existLottery.Length+1);
    Console.Write("Enter new magic number: ");
    int newLottery = int.Parse(Console.ReadLine());
    return existLottery[existLottery.Length -1] = newLottery;
}

void Menu()
{

    Console.WriteLine("------------------------------");
    Console.WriteLine("| 1. Add new Lottery         |");
    Console.WriteLine("| 2. Print all exist Lottery |");
    Console.WriteLine("| 3. Exit                    |");
    Console.WriteLine("------------------------------");
    Console.WriteLine("Enter your choice :");
}

do
{
    Menu();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            Console.WriteLine("------------------------------");
            AddNewLottery(existLottery);
            PrintLottery();
            break;
        case 2:
            Console.WriteLine("------------------------------");
            PrintLottery();
            break;
        case 3:
            Console.WriteLine("BYE BYE !");
            break;
        default:
            break;
    }
} while (choice < 3);