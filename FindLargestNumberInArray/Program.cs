﻿int FindLargestNumber(int[] arr)
{
    int largestNumber = 0;
    for (int i = 0; i < arr.Length; i++)
    {
        if (arr[i] > largestNumber)
        {
            largestNumber = arr[i];
        }
    }
    return largestNumber;
}

int[] array = { 1, 2, 3, 4, 5, 6, 7, 90, 9, 10, 11, 12 };
Console.WriteLine(FindLargestNumber(array));