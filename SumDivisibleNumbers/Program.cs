﻿int SumDivisibleNumbers(int[] numbers, int[] divNumbers)
{
    int sum = 0;
    for(int i=0; i< numbers.Length;i++)
    {
        for(int j=0; j <divNumbers.Length; j++)
        {
            if (numbers[i] % divNumbers[j] ==0)
            {
                sum += numbers[i];
            }
        }
    }
    return sum;
}

int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
int[] divNumbers = { 2, 3 };
Console.WriteLine(SumDivisibleNumbers(numbers,divNumbers));