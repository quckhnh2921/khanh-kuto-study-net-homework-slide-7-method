﻿const int PRICE_OF_FIRST_TEN_ORES = 10;
const int PRICE_OF_NEXT_FIVE_ORES = 5;
const int PRICE_OF_NEXT_THREE_ORES = 2;
const int PRICE_OF_REMAINDER = 1;
int CheckOreAndTurnToGold(int numberOfOre)
{
    //Get first 10 ores and turn it to price
    int firstTenOres = Math.Min(numberOfOre, 10);
    int firstTenOresToGold = firstTenOres * PRICE_OF_FIRST_TEN_ORES;
    numberOfOre -= firstTenOres;

    //Get next 5 ores and turn it to price
    int nextFiveOres = Math.Min(numberOfOre, 5);
    int nextFiveOresToGold = nextFiveOres * PRICE_OF_NEXT_FIVE_ORES;
    numberOfOre -= nextFiveOres;

    //Get next 3 ores and turn it to price
    int nextThreeOres = Math.Min(numberOfOre, 3);
    int nextThreeOresToGold = nextThreeOres * PRICE_OF_NEXT_THREE_ORES;
    numberOfOre -= nextThreeOres;

    //Get the remainder and turn it to price
    int remainder = numberOfOre;
    int nextRemainderToGold = numberOfOre * PRICE_OF_REMAINDER;

    //Total price
    int totalGold = firstTenOresToGold + nextFiveOresToGold + nextThreeOresToGold + nextRemainderToGold;

    return totalGold;
}

Console.Write("Enter number of your ores: ");
int numberOfOre = int.Parse(Console.ReadLine());

Console.WriteLine("You sell "+numberOfOre+" ores and get "+CheckOreAndTurnToGold(numberOfOre)+" gold !");