﻿const int KEO = 1;
const int BUA = 2;
const int BAO = 3;
const int DRAW = 0;
const int PLAYER_ONE_WIN = 1;
const int PLAYER_TWO_WIN = 2;

//game's rule
int Rule(int playerOneChoose, int playerTwoChoose)
{
    if(playerOneChoose == KEO && playerTwoChoose == BAO || playerOneChoose == BUA && playerTwoChoose == KEO || playerOneChoose == BAO && playerTwoChoose == BUA )
    {
        return PLAYER_ONE_WIN;
    }else if (playerTwoChoose == KEO && playerOneChoose == BAO || playerTwoChoose == BUA && playerOneChoose == KEO || playerTwoChoose == BAO && playerOneChoose == BUA)
    {
        return PLAYER_TWO_WIN;
    }else 
    { 
        return DRAW;
    }
}
//game's result
string CheckResult(int result)
{
    if (result == PLAYER_ONE_WIN)
    {
        return "Player 1 win !";
    }
    else if (result == PLAYER_TWO_WIN)
    {
        return "Player 2 win !";
    }
    else
    {
        return "Draw";
    }
}

void DisplayChoose()
{
    Console.WriteLine("KEO : 1");
    Console.WriteLine("BUA : 2");
    Console.WriteLine("BAO : 3");
}

DisplayChoose();
//player input their choice
Console.WriteLine("Player 1 choose: ");
int playerOneChoose = int.Parse(Console.ReadLine());
Console.WriteLine("Player 2 choose: ");
int playerTwoChoose = int.Parse(Console.ReadLine());
//print the result
int checkResult = Rule(playerOneChoose, playerTwoChoose);
Console.WriteLine(CheckResult(checkResult)); 

