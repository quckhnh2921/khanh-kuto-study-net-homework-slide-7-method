﻿int[] arr = { 5, 7, 4, 9 };

int SecondLargestNumber(int[] temp)
{
    int firstLargest = 0;
    int secondLargest = 0;
    foreach(int i in temp)
    {
        if (i> firstLargest)
        {
            secondLargest = firstLargest;
            firstLargest= i;
        }else if (i> secondLargest)
        {
            secondLargest = i;
        }
    }
    return secondLargest;
}

Console.WriteLine(SecondLargestNumber(arr));